FROM python:3.9
WORKDIR /usr/src/app
ENV PORT=8080
ENV USERS=3
ENV ROUND_USER_INTERVAL_MINUTES=30
COPY requirements.txt ./
RUN pip install -r requirements.txt
COPY . ./
CMD ["python3","service.py"]